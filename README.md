Schematics for ITkPixV1 quad hybrid:
- 2/1/0.5/0.25 data sharing
- CMD forwarding from two chips
- Voffset sharing
- Data connector: 39 pin (data, cmd, LPenable, MUX, ChipID, GND)
- Power connector: 16 pin (LV, HV, NTC, LPenable)

v1.0 (2020-07-24):
- Initial version of the schematics

v1.1 (2020-07-27):
- LPenable AC coupled to each chip separately
- added a GND on the data connector next to ChipID

v1.2 (2020-07-30):
- added external termination for data input lines (do not load for ITkPixV1)
- data sharing option between 2 and 0.25 links per FE can be selected with wirebonds or solder jumper

v1.3 (2020-08-15):
- data pinout corrected for Chip1 and Chip3 (GTX0->GTX1 switch)
- Voffset corrected
- Added a footprint with RD53B pads

v1.4 (2020-08-25):
- Vina-GND and Vind-GND capacitors replaced with one Vin-GND capacitor
- Voffset corrected
- Renaming of differential pair nets so that they end with "P" and "N"

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 20000 11000
encoding utf-8
Sheet 3 5
Title "ITkPixV1 Quad Hybrid"
Date "2020-08-25"
Rev "1.4"
Comp "LBNL"
Comment1 "cross-check: Timon Heim, Maurice Garcia-Sciveres "
Comment2 "design: Aleksandra Dimitrievska"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L rd53b_hybrid_wirebonds:RD53B U2
U 1 1 5FA65DB9
P 9700 4250
AR Path="/5F0F4300/5FA65DB9" Ref="U2"  Part="1" 
AR Path="/5F0F435C/5FA65DB9" Ref="U3"  Part="1" 
AR Path="/5F0F439D/5FA65DB9" Ref="U4"  Part="1" 
AR Path="/5F0F427A/5FA65DB9" Ref="U5"  Part="1" 
F 0 "U4" H 10550 5850 500 0000 C CNN
F 1 "RD53B" H 10600 4750 500 0000 C CNN
F 2 "Library:rd53b_hybrid_wirebonds_Chip4" H 5950 2400 60  0001 C CNN
F 3 "" H 5950 2400 60  0001 C CNN
	1    9700 4250
	1    0    0    -1  
$EndComp
Text HLabel 9200 9800 3    50   Input ~ 0
CMD_P
Wire Wire Line
	9200 8450 9200 9800
Wire Wire Line
	9350 8450 9350 9800
Text HLabel 9350 9800 3    50   Input ~ 0
CMD_N
Wire Wire Line
	2200 850  1900 850 
Text Notes 600  650  0    50   ~ 0
optional, can be just a wirebond to the GND\n
$Comp
L Device:Thermistor_NTC TH2
U 1 1 5FA65DBA
P 1900 1000
AR Path="/5F0F4300/5FA65DBA" Ref="TH2"  Part="1" 
AR Path="/5F0F435C/5FA65DBA" Ref="TH3"  Part="1" 
AR Path="/5F0F439D/5FA65DBA" Ref="TH4"  Part="1" 
AR Path="/5F0F427A/5FA65DBA" Ref="TH5"  Part="1" 
F 0 "TH4" V 1950 1200 50  0000 C CNN
F 1 "10k" V 1800 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0201_NoSilk" H 1900 1050 50  0001 C CNN
F 3 "~" H 1900 1050 50  0001 C CNN
	1    1900 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 1000 1350 1000
Text Label 1350 1000 0    50   ~ 0
GNDA_REF_A
Wire Wire Line
	2200 1300 1750 1300
Text Label 1750 1300 0    50   ~ 0
GNDA_REF_A
Wire Wire Line
	2050 1000 2200 1000
$Comp
L Device:C C31
U 1 1 5FA65DBB
P 1250 1050
AR Path="/5F0F4300/5FA65DBB" Ref="C31"  Part="1" 
AR Path="/5F0F435C/5FA65DBB" Ref="C50"  Part="1" 
AR Path="/5F0F439D/5FA65DBB" Ref="C69"  Part="1" 
AR Path="/5F0F427A/5FA65DBB" Ref="C88"  Part="1" 
F 0 "C69" V 1200 900 50  0000 C CNN
F 1 "100n" V 1100 1050 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 1288 900 50  0001 C CNN
F 3 "~" H 1250 1050 50  0001 C CNN
	1    1250 1050
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 5FA65F1A
P 1250 1250
AR Path="/5F0F4300/5FA65F1A" Ref="R17"  Part="1" 
AR Path="/5F0F435C/5FA65F1A" Ref="R26"  Part="1" 
AR Path="/5F0F439D/5FA65F1A" Ref="R35"  Part="1" 
AR Path="/5F0F427A/5FA65F1A" Ref="R44"  Part="1" 
F 0 "R35" V 1200 1100 50  0000 C CNN
F 1 "84.5k" V 1350 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 1180 1250 50  0001 C CNN
F 3 "~" H 1250 1250 50  0001 C CNN
	1    1250 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	1400 1050 1500 1050
Wire Wire Line
	1500 1250 1400 1250
Wire Wire Line
	1100 1050 1000 1050
Wire Wire Line
	1000 1050 1000 1150
Wire Wire Line
	1000 1250 1100 1250
Wire Wire Line
	1000 1150 600  1150
Connection ~ 1000 1150
Wire Wire Line
	1000 1150 1000 1250
Text Label 600  1150 0    50   ~ 0
GNDA_REF_A
Wire Wire Line
	1500 1050 1500 1150
Wire Wire Line
	1500 1150 2200 1150
Connection ~ 1500 1150
Wire Wire Line
	1500 1150 1500 1250
Text Label 1950 1450 0    50   ~ 0
VMUX
$Comp
L Device:C C32
U 1 1 5FA65F1B
P 1250 1600
AR Path="/5F0F4300/5FA65F1B" Ref="C32"  Part="1" 
AR Path="/5F0F435C/5FA65F1B" Ref="C51"  Part="1" 
AR Path="/5F0F439D/5FA65F1B" Ref="C70"  Part="1" 
AR Path="/5F0F427A/5FA65F1B" Ref="C89"  Part="1" 
F 0 "C70" V 1200 1700 50  0000 C CNN
F 1 "22n" V 1100 1600 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 1288 1450 50  0001 C CNN
F 3 "~" H 1250 1600 50  0001 C CNN
	1    1250 1600
	0    1    1    0   
$EndComp
Text Label 600  1600 0    50   ~ 0
GNDA_REF_A
Text HLabel 1600 1500 2    50   Input ~ 0
MUX
Text Label 1500 1150 0    50   ~ 0
VREF_ADC
Text Label 2050 1000 0    50   ~ 0
NTC_Chip
Wire Wire Line
	2200 1900 1950 1900
Text Label 1950 1900 0    50   ~ 0
GND
$Comp
L power:GND #PWR02
U 1 1 5FA65F1C
P 700 10200
AR Path="/5F0F4300/5FA65F1C" Ref="#PWR02"  Part="1" 
AR Path="/5F0F435C/5FA65F1C" Ref="#PWR03"  Part="1" 
AR Path="/5F0F439D/5FA65F1C" Ref="#PWR04"  Part="1" 
AR Path="/5F0F427A/5FA65F1C" Ref="#PWR05"  Part="1" 
F 0 "#PWR04" H 700 9950 50  0001 C CNN
F 1 "GND" H 705 10027 50  0000 C CNN
F 2 "" H 700 10200 50  0001 C CNN
F 3 "" H 700 10200 50  0001 C CNN
	1    700  10200
	1    0    0    -1  
$EndComp
Text Label 850  10200 0    50   ~ 0
GND
Text Notes 600  8600 0    50   ~ 0
Power
Wire Wire Line
	2200 2050 1950 2050
Wire Wire Line
	1950 2050 1950 2200
Wire Wire Line
	1950 2650 2200 2650
Wire Wire Line
	2200 2500 1950 2500
Connection ~ 1950 2500
Wire Wire Line
	1950 2500 1950 2650
Wire Wire Line
	2200 2350 1950 2350
Connection ~ 1950 2350
Wire Wire Line
	1950 2350 1950 2500
Wire Wire Line
	2200 2200 1950 2200
Connection ~ 1950 2200
Wire Wire Line
	1950 2200 1950 2350
Text Label 1650 2350 0    50   ~ 0
GND
Wire Wire Line
	8900 8450 8900 8700
Text Label 8900 8700 1    50   ~ 0
GND
Wire Wire Line
	18300 1250 18550 1250
Text Label 18550 1250 2    50   ~ 0
GND
Wire Wire Line
	2200 5800 1950 5800
Wire Wire Line
	1950 5800 1950 5950
Wire Wire Line
	1950 6400 2200 6400
Wire Wire Line
	2200 6250 1950 6250
Connection ~ 1950 6250
Wire Wire Line
	1950 6250 1950 6400
Wire Wire Line
	2200 6100 1950 6100
Connection ~ 1950 6100
Wire Wire Line
	1950 6100 1950 6250
Wire Wire Line
	2200 5950 1950 5950
Connection ~ 1950 5950
Wire Wire Line
	1950 5950 1950 6100
Text Label 1650 6100 0    50   ~ 0
GND
Wire Wire Line
	4400 8450 4400 8700
Wire Wire Line
	4400 8700 4550 8700
Wire Wire Line
	5000 8700 5000 8450
Wire Wire Line
	4850 8450 4850 8700
Connection ~ 4850 8700
Wire Wire Line
	4850 8700 5000 8700
Wire Wire Line
	4700 8450 4700 8700
Connection ~ 4700 8700
Wire Wire Line
	4700 8700 4850 8700
Wire Wire Line
	4550 8450 4550 8700
Connection ~ 4550 8700
Wire Wire Line
	4550 8700 4700 8700
Text Label 4700 9000 1    50   ~ 0
GND
Wire Wire Line
	8150 8450 8150 8700
Wire Wire Line
	8150 8700 8300 8700
Wire Wire Line
	8750 8700 8750 8450
Wire Wire Line
	8600 8450 8600 8700
Connection ~ 8600 8700
Wire Wire Line
	8600 8700 8750 8700
Wire Wire Line
	8450 8450 8450 8700
Connection ~ 8450 8700
Wire Wire Line
	8450 8700 8600 8700
Wire Wire Line
	8300 8450 8300 8700
Connection ~ 8300 8700
Wire Wire Line
	8300 8700 8450 8700
Text Label 8450 9000 1    50   ~ 0
GND
Wire Wire Line
	12200 8450 12200 8700
Wire Wire Line
	12200 8700 12350 8700
Wire Wire Line
	12800 8700 12800 8450
Wire Wire Line
	12650 8450 12650 8700
Connection ~ 12650 8700
Wire Wire Line
	12650 8700 12800 8700
Wire Wire Line
	12500 8450 12500 8700
Connection ~ 12500 8700
Wire Wire Line
	12500 8700 12650 8700
Wire Wire Line
	12350 8450 12350 8700
Connection ~ 12350 8700
Wire Wire Line
	12350 8700 12500 8700
Text Label 12500 9000 1    50   ~ 0
GND
Wire Wire Line
	15950 8450 15950 8700
Wire Wire Line
	15950 8700 16100 8700
Wire Wire Line
	16550 8700 16550 8450
Wire Wire Line
	16400 8450 16400 8700
Connection ~ 16400 8700
Wire Wire Line
	16400 8700 16550 8700
Wire Wire Line
	16250 8450 16250 8700
Connection ~ 16250 8700
Wire Wire Line
	16250 8700 16400 8700
Wire Wire Line
	16100 8450 16100 8700
Connection ~ 16100 8700
Wire Wire Line
	16100 8700 16250 8700
Text Label 16250 9000 1    50   ~ 0
GND
Wire Wire Line
	18300 5750 18550 5750
Wire Wire Line
	18550 5750 18550 5600
Wire Wire Line
	18550 5150 18300 5150
Wire Wire Line
	18300 5300 18550 5300
Connection ~ 18550 5300
Wire Wire Line
	18550 5300 18550 5150
Wire Wire Line
	18300 5450 18550 5450
Connection ~ 18550 5450
Wire Wire Line
	18550 5450 18550 5300
Wire Wire Line
	18300 5600 18550 5600
Connection ~ 18550 5600
Wire Wire Line
	18550 5600 18550 5450
Text Label 18850 5450 2    50   ~ 0
GND
Wire Wire Line
	18300 2000 18550 2000
Wire Wire Line
	18550 2000 18550 1850
Wire Wire Line
	18550 1400 18300 1400
Wire Wire Line
	18300 1550 18550 1550
Connection ~ 18550 1550
Wire Wire Line
	18550 1550 18550 1400
Wire Wire Line
	18300 1700 18550 1700
Connection ~ 18550 1700
Wire Wire Line
	18550 1700 18550 1550
Wire Wire Line
	18300 1850 18550 1850
Connection ~ 18550 1850
Wire Wire Line
	18550 1850 18550 1700
Text Label 18850 1700 2    50   ~ 0
GND
Wire Wire Line
	2200 2800 1950 2800
Wire Wire Line
	1950 2800 1950 2950
Wire Wire Line
	1950 3400 2200 3400
Wire Wire Line
	2200 3250 1950 3250
Connection ~ 1950 3250
Wire Wire Line
	1950 3250 1950 3400
Wire Wire Line
	2200 3100 1950 3100
Connection ~ 1950 3100
Wire Wire Line
	1950 3100 1950 3250
Wire Wire Line
	2200 2950 1950 2950
Connection ~ 1950 2950
Wire Wire Line
	1950 2950 1950 3100
Wire Wire Line
	1950 3100 1650 3100
Text Label 1650 3100 0    50   ~ 0
VDDA1
$Comp
L Device:C C33
U 1 1 5FA65F1D
P 1500 3100
AR Path="/5F0F4300/5FA65F1D" Ref="C33"  Part="1" 
AR Path="/5F0F435C/5FA65F1D" Ref="C52"  Part="1" 
AR Path="/5F0F439D/5FA65F1D" Ref="C71"  Part="1" 
AR Path="/5F0F427A/5FA65F1D" Ref="C90"  Part="1" 
F 0 "C71" V 1248 3100 50  0000 C CNN
F 1 "560n" V 1339 3100 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 1538 2950 50  0001 C CNN
F 3 "~" H 1500 3100 50  0001 C CNN
	1    1500 3100
	0    1    1    0   
$EndComp
Text Notes 2700 4300 0    50   ~ 0
VinA/D 4? cap to GND\n(influences ac-impedance of SP chain)
Wire Wire Line
	1350 3100 1100 3100
Text Label 1100 3100 0    50   ~ 0
GND
Wire Wire Line
	2200 5050 1950 5050
Wire Wire Line
	1950 5050 1950 5200
Wire Wire Line
	1950 5650 2200 5650
Wire Wire Line
	2200 5500 1950 5500
Connection ~ 1950 5500
Wire Wire Line
	1950 5500 1950 5650
Wire Wire Line
	2200 5350 1950 5350
Connection ~ 1950 5350
Wire Wire Line
	1950 5350 1950 5500
Wire Wire Line
	2200 5200 1950 5200
Connection ~ 1950 5200
Wire Wire Line
	1950 5200 1950 5350
Wire Wire Line
	1950 5350 1650 5350
Text Label 1650 5350 0    50   ~ 0
VDDD1
$Comp
L Device:C C35
U 1 1 5FA65F1E
P 1500 5350
AR Path="/5F0F4300/5FA65F1E" Ref="C35"  Part="1" 
AR Path="/5F0F435C/5FA65F1E" Ref="C54"  Part="1" 
AR Path="/5F0F439D/5FA65F1E" Ref="C73"  Part="1" 
AR Path="/5F0F427A/5FA65F1E" Ref="C92"  Part="1" 
F 0 "C73" V 1248 5350 50  0000 C CNN
F 1 "560n" V 1339 5350 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 1538 5200 50  0001 C CNN
F 3 "~" H 1500 5350 50  0001 C CNN
	1    1500 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	1350 5350 1100 5350
Text Label 1100 5350 0    50   ~ 0
GND
Wire Wire Line
	2200 3550 1950 3550
Wire Wire Line
	1950 3550 1950 3700
Wire Wire Line
	1950 4150 2200 4150
Wire Wire Line
	2200 4000 1950 4000
Connection ~ 1950 4000
Wire Wire Line
	1950 4000 1950 4150
Wire Wire Line
	2200 3850 1950 3850
Connection ~ 1950 3850
Wire Wire Line
	1950 3850 1950 4000
Wire Wire Line
	2200 3700 1950 3700
Connection ~ 1950 3700
Wire Wire Line
	1950 3700 1950 3850
Wire Wire Line
	2200 4300 1950 4300
Wire Wire Line
	1950 4300 1950 4450
Wire Wire Line
	1950 4900 2200 4900
Wire Wire Line
	2200 4750 1950 4750
Connection ~ 1950 4750
Wire Wire Line
	1950 4750 1950 4900
Wire Wire Line
	2200 4600 1950 4600
Connection ~ 1950 4600
Wire Wire Line
	1950 4600 1950 4750
Wire Wire Line
	2200 4450 1950 4450
Connection ~ 1950 4450
Wire Wire Line
	1950 4450 1950 4600
$Comp
L Device:C C34
U 1 1 5F0C661E
P 1500 4300
AR Path="/5F0F4300/5F0C661E" Ref="C34"  Part="1" 
AR Path="/5F0F435C/5F0C661E" Ref="C53"  Part="1" 
AR Path="/5F0F439D/5F0C661E" Ref="C72"  Part="1" 
AR Path="/5F0F427A/5F0C661E" Ref="C91"  Part="1" 
F 0 "C72" V 1248 4300 50  0000 C CNN
F 1 "4.7u" V 1339 4300 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 1538 4150 50  0001 C CNN
F 3 "~" H 1500 4300 50  0001 C CNN
	1    1500 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 8450 5150 8700
Wire Wire Line
	5150 8700 5300 8700
Wire Wire Line
	5750 8700 5750 8450
Wire Wire Line
	5600 8450 5600 8700
Connection ~ 5600 8700
Wire Wire Line
	5600 8700 5750 8700
Wire Wire Line
	5450 8450 5450 8700
Connection ~ 5450 8700
Wire Wire Line
	5450 8700 5600 8700
Wire Wire Line
	5300 8450 5300 8700
Connection ~ 5300 8700
Wire Wire Line
	5300 8700 5450 8700
Wire Wire Line
	5450 8700 5450 9000
Text Label 5450 9000 1    50   ~ 0
VDDA2
$Comp
L Device:C C39
U 1 1 5FA65DC2
P 5450 9150
AR Path="/5F0F4300/5FA65DC2" Ref="C39"  Part="1" 
AR Path="/5F0F435C/5FA65DC2" Ref="C58"  Part="1" 
AR Path="/5F0F439D/5FA65DC2" Ref="C77"  Part="1" 
AR Path="/5F0F427A/5FA65DC2" Ref="C96"  Part="1" 
F 0 "C77" V 5198 9150 50  0000 C CNN
F 1 "560n" V 5289 9150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 5488 9000 50  0001 C CNN
F 3 "~" H 5450 9150 50  0001 C CNN
	1    5450 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 9300 5450 9550
Text Label 5450 9550 1    50   ~ 0
GND
Wire Wire Line
	7400 8450 7400 8700
Wire Wire Line
	7400 8700 7550 8700
Wire Wire Line
	8000 8700 8000 8450
Wire Wire Line
	7850 8450 7850 8700
Connection ~ 7850 8700
Wire Wire Line
	7850 8700 8000 8700
Wire Wire Line
	7700 8450 7700 8700
Connection ~ 7700 8700
Wire Wire Line
	7700 8700 7850 8700
Wire Wire Line
	7550 8450 7550 8700
Connection ~ 7550 8700
Wire Wire Line
	7550 8700 7700 8700
Wire Wire Line
	7700 8700 7700 9000
Text Label 7700 9000 1    50   ~ 0
VDDD2
$Comp
L Device:C C41
U 1 1 5FA65DC3
P 7700 9150
AR Path="/5F0F4300/5FA65DC3" Ref="C41"  Part="1" 
AR Path="/5F0F435C/5FA65DC3" Ref="C60"  Part="1" 
AR Path="/5F0F439D/5FA65DC3" Ref="C79"  Part="1" 
AR Path="/5F0F427A/5FA65DC3" Ref="C98"  Part="1" 
F 0 "C79" V 7448 9150 50  0000 C CNN
F 1 "560n" V 7539 9150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 7738 9000 50  0001 C CNN
F 3 "~" H 7700 9150 50  0001 C CNN
	1    7700 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 9300 7700 9550
Text Label 7700 9550 1    50   ~ 0
GND
Wire Wire Line
	5900 8450 5900 8700
Wire Wire Line
	5900 8700 6050 8700
Wire Wire Line
	6500 8700 6500 8450
Wire Wire Line
	6350 8450 6350 8700
Connection ~ 6350 8700
Wire Wire Line
	6350 8700 6500 8700
Wire Wire Line
	6200 8450 6200 8700
Connection ~ 6200 8700
Wire Wire Line
	6200 8700 6350 8700
Wire Wire Line
	6050 8450 6050 8700
Connection ~ 6050 8700
Wire Wire Line
	6050 8700 6200 8700
Wire Wire Line
	6650 8450 6650 8700
Wire Wire Line
	6650 8700 6800 8700
Wire Wire Line
	7250 8700 7250 8450
Wire Wire Line
	7100 8450 7100 8700
Connection ~ 7100 8700
Wire Wire Line
	7100 8700 7250 8700
Wire Wire Line
	6950 8450 6950 8700
Connection ~ 6950 8700
Wire Wire Line
	6950 8700 7100 8700
Wire Wire Line
	6800 8450 6800 8700
Connection ~ 6800 8700
Wire Wire Line
	6800 8700 6950 8700
$Comp
L Device:C C40
U 1 1 5F0CE0CD
P 6650 9150
AR Path="/5F0F4300/5F0CE0CD" Ref="C40"  Part="1" 
AR Path="/5F0F435C/5F0CE0CD" Ref="C59"  Part="1" 
AR Path="/5F0F439D/5F0CE0CD" Ref="C78"  Part="1" 
AR Path="/5F0F427A/5F0CE0CD" Ref="C97"  Part="1" 
F 0 "C78" V 6398 9150 50  0000 C CNN
F 1 "4.7u" V 6489 9150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 6688 9000 50  0001 C CNN
F 3 "~" H 6650 9150 50  0001 C CNN
	1    6650 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 8450 12950 8700
Wire Wire Line
	12950 8700 13100 8700
Wire Wire Line
	13550 8700 13550 8450
Wire Wire Line
	13400 8450 13400 8700
Connection ~ 13400 8700
Wire Wire Line
	13400 8700 13550 8700
Wire Wire Line
	13250 8450 13250 8700
Connection ~ 13250 8700
Wire Wire Line
	13250 8700 13400 8700
Wire Wire Line
	13100 8450 13100 8700
Connection ~ 13100 8700
Wire Wire Line
	13100 8700 13250 8700
Wire Wire Line
	13250 8700 13250 9000
Text Label 13250 9000 1    50   ~ 0
VDDA3
$Comp
L Device:C C42
U 1 1 5FA65DC5
P 13250 9150
AR Path="/5F0F4300/5FA65DC5" Ref="C42"  Part="1" 
AR Path="/5F0F435C/5FA65DC5" Ref="C61"  Part="1" 
AR Path="/5F0F439D/5FA65DC5" Ref="C80"  Part="1" 
AR Path="/5F0F427A/5FA65DC5" Ref="C99"  Part="1" 
F 0 "C80" V 12998 9150 50  0000 C CNN
F 1 "560n" V 13089 9150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 13288 9000 50  0001 C CNN
F 3 "~" H 13250 9150 50  0001 C CNN
	1    13250 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	13250 9300 13250 9550
Text Label 13250 9550 1    50   ~ 0
GND
Wire Wire Line
	15200 8450 15200 8700
Wire Wire Line
	15200 8700 15350 8700
Wire Wire Line
	15800 8700 15800 8450
Wire Wire Line
	15650 8450 15650 8700
Connection ~ 15650 8700
Wire Wire Line
	15650 8700 15800 8700
Wire Wire Line
	15500 8450 15500 8700
Connection ~ 15500 8700
Wire Wire Line
	15500 8700 15650 8700
Wire Wire Line
	15350 8450 15350 8700
Connection ~ 15350 8700
Wire Wire Line
	15350 8700 15500 8700
Wire Wire Line
	15500 8700 15500 9000
Text Label 15500 9000 1    50   ~ 0
VDDD3
$Comp
L Device:C C44
U 1 1 5FA65DC6
P 15500 9150
AR Path="/5F0F4300/5FA65DC6" Ref="C44"  Part="1" 
AR Path="/5F0F435C/5FA65DC6" Ref="C63"  Part="1" 
AR Path="/5F0F439D/5FA65DC6" Ref="C82"  Part="1" 
AR Path="/5F0F427A/5FA65DC6" Ref="C101"  Part="1" 
F 0 "C82" V 15248 9150 50  0000 C CNN
F 1 "560n" V 15339 9150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 15538 9000 50  0001 C CNN
F 3 "~" H 15500 9150 50  0001 C CNN
	1    15500 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	15500 9300 15500 9550
Text Label 15500 9550 1    50   ~ 0
GND
Wire Wire Line
	13700 8450 13700 8700
Wire Wire Line
	13700 8700 13850 8700
Wire Wire Line
	14300 8700 14300 8450
Wire Wire Line
	14150 8450 14150 8700
Connection ~ 14150 8700
Wire Wire Line
	14150 8700 14300 8700
Wire Wire Line
	14000 8450 14000 8700
Connection ~ 14000 8700
Wire Wire Line
	14000 8700 14150 8700
Wire Wire Line
	13850 8450 13850 8700
Connection ~ 13850 8700
Wire Wire Line
	13850 8700 14000 8700
Wire Wire Line
	14450 8450 14450 8700
Wire Wire Line
	14450 8700 14600 8700
Wire Wire Line
	15050 8700 15050 8450
Wire Wire Line
	14900 8450 14900 8700
Connection ~ 14900 8700
Wire Wire Line
	14900 8700 15050 8700
Wire Wire Line
	14750 8450 14750 8700
Connection ~ 14750 8700
Wire Wire Line
	14750 8700 14900 8700
Wire Wire Line
	14600 8450 14600 8700
Connection ~ 14600 8700
Wire Wire Line
	14600 8700 14750 8700
$Comp
L Device:C C43
U 1 1 5FA65DC7
P 14450 9150
AR Path="/5F0F4300/5FA65DC7" Ref="C43"  Part="1" 
AR Path="/5F0F435C/5FA65DC7" Ref="C62"  Part="1" 
AR Path="/5F0F439D/5FA65DC7" Ref="C81"  Part="1" 
AR Path="/5F0F427A/5FA65DC7" Ref="C100"  Part="1" 
F 0 "C81" V 14198 9150 50  0000 C CNN
F 1 "4.7u" V 14289 9150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 14488 9000 50  0001 C CNN
F 3 "~" H 14450 9150 50  0001 C CNN
	1    14450 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	18300 5000 18550 5000
Wire Wire Line
	18550 5000 18550 4850
Wire Wire Line
	18550 4400 18300 4400
Wire Wire Line
	18300 4550 18550 4550
Connection ~ 18550 4550
Wire Wire Line
	18550 4550 18550 4400
Wire Wire Line
	18300 4700 18550 4700
Connection ~ 18550 4700
Wire Wire Line
	18550 4700 18550 4550
Wire Wire Line
	18300 4850 18550 4850
Connection ~ 18550 4850
Wire Wire Line
	18550 4850 18550 4700
Wire Wire Line
	18550 4700 18850 4700
Text Label 18850 4700 2    50   ~ 0
VDDA4
$Comp
L Device:C C49
U 1 1 5FA65DC8
P 19000 4700
AR Path="/5F0F4300/5FA65DC8" Ref="C49"  Part="1" 
AR Path="/5F0F435C/5FA65DC8" Ref="C68"  Part="1" 
AR Path="/5F0F439D/5FA65DC8" Ref="C87"  Part="1" 
AR Path="/5F0F427A/5FA65DC8" Ref="C106"  Part="1" 
F 0 "C87" V 18748 4700 50  0000 C CNN
F 1 "560n" V 18839 4700 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 19038 4550 50  0001 C CNN
F 3 "~" H 19000 4700 50  0001 C CNN
	1    19000 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	19150 4700 19400 4700
Text Label 19400 4700 2    50   ~ 0
GND
Wire Wire Line
	18300 2750 18550 2750
Wire Wire Line
	18550 2750 18550 2600
Wire Wire Line
	18550 2150 18300 2150
Wire Wire Line
	18300 2300 18550 2300
Connection ~ 18550 2300
Wire Wire Line
	18550 2300 18550 2150
Wire Wire Line
	18300 2450 18550 2450
Connection ~ 18550 2450
Wire Wire Line
	18550 2450 18550 2300
Wire Wire Line
	18300 2600 18550 2600
Connection ~ 18550 2600
Wire Wire Line
	18550 2600 18550 2450
Wire Wire Line
	18550 2450 18850 2450
Text Label 18850 2450 2    50   ~ 0
VDDD4
$Comp
L Device:C C47
U 1 1 5FA65DC9
P 19000 2450
AR Path="/5F0F4300/5FA65DC9" Ref="C47"  Part="1" 
AR Path="/5F0F435C/5FA65DC9" Ref="C66"  Part="1" 
AR Path="/5F0F439D/5FA65DC9" Ref="C85"  Part="1" 
AR Path="/5F0F427A/5FA65DC9" Ref="C104"  Part="1" 
F 0 "C85" V 18748 2450 50  0000 C CNN
F 1 "560n" V 18839 2450 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 19038 2300 50  0001 C CNN
F 3 "~" H 19000 2450 50  0001 C CNN
	1    19000 2450
	0    -1   -1   0   
$EndComp
Text Label 19400 2450 2    50   ~ 0
GND
Wire Wire Line
	18300 4250 18550 4250
Wire Wire Line
	18550 4250 18550 4100
Wire Wire Line
	18550 3650 18300 3650
Wire Wire Line
	18300 3800 18550 3800
Connection ~ 18550 3800
Wire Wire Line
	18550 3800 18550 3650
Wire Wire Line
	18300 3950 18550 3950
Connection ~ 18550 3950
Wire Wire Line
	18550 3950 18550 3800
Wire Wire Line
	18300 4100 18550 4100
Connection ~ 18550 4100
Wire Wire Line
	18550 4100 18550 3950
Wire Wire Line
	18300 3500 18550 3500
Wire Wire Line
	18550 3500 18550 3350
Wire Wire Line
	18550 2900 18300 2900
Wire Wire Line
	18300 3050 18550 3050
Connection ~ 18550 3050
Wire Wire Line
	18550 3050 18550 2900
Wire Wire Line
	18300 3200 18550 3200
Connection ~ 18550 3200
Wire Wire Line
	18550 3200 18550 3050
Wire Wire Line
	18300 3350 18550 3350
Connection ~ 18550 3350
Wire Wire Line
	18550 3350 18550 3200
$Comp
L Device:C C48
U 1 1 5FA65DCA
P 19000 3500
AR Path="/5F0F4300/5FA65DCA" Ref="C48"  Part="1" 
AR Path="/5F0F435C/5FA65DCA" Ref="C67"  Part="1" 
AR Path="/5F0F439D/5FA65DCA" Ref="C86"  Part="1" 
AR Path="/5F0F427A/5FA65DCA" Ref="C105"  Part="1" 
F 0 "C86" V 18748 3500 50  0000 C CNN
F 1 "4.7u" V 18839 3500 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 19038 3350 50  0001 C CNN
F 3 "~" H 19000 3500 50  0001 C CNN
	1    19000 3500
	0    -1   -1   0   
$EndComp
NoConn ~ 2200 7000
Wire Wire Line
	2200 7300 1950 7300
Wire Wire Line
	1950 7300 1950 7150
Wire Wire Line
	1950 7150 2200 7150
Text Notes 650  7350 0    50   ~ 0
ChipID\n(internal pull-up to Vddd)
Wire Wire Line
	2200 7900 1950 7900
Wire Wire Line
	1950 7900 1950 8050
Wire Wire Line
	1950 8200 2200 8200
Wire Wire Line
	2200 8050 1950 8050
Connection ~ 1950 8050
Wire Wire Line
	1950 8050 1950 8200
Wire Wire Line
	1950 7900 1950 7750
Wire Wire Line
	1950 7750 2200 7750
Connection ~ 1950 7900
Text Label 1950 8000 2    50   ~ 0
GND
Text Notes 750  8150 0    50   ~ 0
Iref Trim\n(internal pull-up to Vdd_PRE)
Wire Wire Line
	1350 10200 1550 10200
Text Label 1550 10200 2    50   ~ 0
VIN
Text HLabel 1350 10200 0    50   Input ~ 0
VIN
Wire Wire Line
	2900 8900 2900 9250
Wire Wire Line
	3050 8450 3050 8850
Text Label 2900 9250 1    50   ~ 0
GND
Text Label 3050 8850 1    50   ~ 0
GNDA_REF
$Comp
L Device:C C37
U 1 1 5FA65DCB
P 2900 8750
AR Path="/5F0F4300/5FA65DCB" Ref="C37"  Part="1" 
AR Path="/5F0F435C/5FA65DCB" Ref="C56"  Part="1" 
AR Path="/5F0F439D/5FA65DCB" Ref="C75"  Part="1" 
AR Path="/5F0F427A/5FA65DCB" Ref="C94"  Part="1" 
F 0 "C75" H 2750 8850 50  0000 L CNN
F 1 "2.2u" H 2600 8750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 2938 8600 50  0001 C CNN
F 3 "~" H 2900 8750 50  0001 C CNN
	1    2900 8750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 8450 2900 8600
$Comp
L Device:R R19
U 1 1 5FA65DCC
P 3200 9050
AR Path="/5F0F4300/5FA65DCC" Ref="R19"  Part="1" 
AR Path="/5F0F435C/5FA65DCC" Ref="R28"  Part="1" 
AR Path="/5F0F439D/5FA65DCC" Ref="R37"  Part="1" 
AR Path="/5F0F427A/5FA65DCC" Ref="R46"  Part="1" 
F 0 "R37" V 3100 9050 50  0000 C CNN
F 1 "24.9k" V 3300 9050 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 3130 9050 50  0001 C CNN
F 3 "~" H 3200 9050 50  0001 C CNN
	1    3200 9050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 9200 3200 9700
Text Label 3200 9700 1    50   ~ 0
GNDA_REF
Wire Wire Line
	3200 8450 3200 8900
Text Label 3200 8800 1    50   ~ 0
R_IREF
NoConn ~ 9050 8450
Text Notes 9000 7600 0    50   ~ 0
Don’t connect PLL_VCTRL\n(internal to 800 mV)
NoConn ~ 16700 8450
NoConn ~ 17450 8450
NoConn ~ 18300 6050
NoConn ~ 18300 6200
NoConn ~ 18300 6350
NoConn ~ 18300 6500
Text Notes 17500 9200 0    50   ~ 0
EXT_POR_CAP no need to connect in normal opation \nit provides reset only in bypass mode only\nshould be connected to Vddd with 4.7n
Wire Wire Line
	17550 8450 19050 8450
Wire Wire Line
	18300 8150 19050 8150
Wire Wire Line
	18300 8000 19050 8000
Text Notes 2850 1700 0    50   ~ 0
C_VMUX \nwhen reducing number of different components try with 100n
Text Notes 15200 1200 0    50   ~ 0
EFUSE: connect to GND\n(the chips should be already programmed during wafer probing)
Text Label 19350 1100 2    50   ~ 0
GND
$Comp
L Device:C C38
U 1 1 5FA65F26
P 3400 9800
AR Path="/5F0F4300/5FA65F26" Ref="C38"  Part="1" 
AR Path="/5F0F435C/5FA65F26" Ref="C57"  Part="1" 
AR Path="/5F0F439D/5FA65F26" Ref="C76"  Part="1" 
AR Path="/5F0F427A/5FA65F26" Ref="C95"  Part="1" 
F 0 "C76" V 3350 9650 50  0000 C CNN
F 1 "100n" V 3250 9800 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 3438 9650 50  0001 C CNN
F 3 "~" H 3400 9800 50  0001 C CNN
	1    3400 9800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5FA65F27
P 3600 9800
AR Path="/5F0F4300/5FA65F27" Ref="R21"  Part="1" 
AR Path="/5F0F435C/5FA65F27" Ref="R30"  Part="1" 
AR Path="/5F0F439D/5FA65F27" Ref="R39"  Part="1" 
AR Path="/5F0F427A/5FA65F27" Ref="R48"  Part="1" 
F 0 "R39" V 3550 9650 50  0000 C CNN
F 1 "30k" V 3700 9800 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 3530 9800 50  0001 C CNN
F 3 "~" H 3600 9800 50  0001 C CNN
	1    3600 9800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 9950 3400 10050
Wire Wire Line
	3400 10050 3500 10050
Wire Wire Line
	3600 10050 3600 9950
Wire Wire Line
	3500 10050 3500 10450
Connection ~ 3500 10050
Wire Wire Line
	3500 10050 3600 10050
Text Label 3500 10450 1    50   ~ 0
GNDA_REF
Wire Wire Line
	3400 9650 3400 9550
Wire Wire Line
	3600 9550 3600 9650
Text Label 3500 9550 1    50   ~ 0
VREFA
$Comp
L Device:R R20
U 1 1 5FA65F28
P 3500 8750
AR Path="/5F0F4300/5FA65F28" Ref="R20"  Part="1" 
AR Path="/5F0F435C/5FA65F28" Ref="R29"  Part="1" 
AR Path="/5F0F439D/5FA65F28" Ref="R38"  Part="1" 
AR Path="/5F0F427A/5FA65F28" Ref="R47"  Part="1" 
F 0 "R38" H 3350 8800 50  0000 L CNN
F 1 "400" H 3300 8700 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 3430 8750 50  0001 C CNN
F 3 "~" H 3500 8750 50  0001 C CNN
	1    3500 8750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 9550 3500 9550
Wire Wire Line
	3350 9300 3500 9300
Wire Wire Line
	3500 9300 3500 9550
Wire Wire Line
	3350 8450 3350 9300
Connection ~ 3500 9550
Wire Wire Line
	3500 9550 3600 9550
Wire Wire Line
	3500 8450 3500 8600
Text Label 3500 9250 1    50   ~ 0
VIN
Text Label 3500 8600 1    50   ~ 0
REXTA
Wire Wire Line
	3500 8900 3500 9250
Text Notes 650  8750 0    50   ~ 0
REXTA = 575 (devide by 2 from RD53A)
Wire Wire Line
	3950 9050 3950 8450
$Comp
L Device:R R22
U 1 1 5FA65DD4
P 3800 9600
AR Path="/5F0F4300/5FA65DD4" Ref="R22"  Part="1" 
AR Path="/5F0F435C/5FA65DD4" Ref="R31"  Part="1" 
AR Path="/5F0F439D/5FA65DD4" Ref="R40"  Part="1" 
AR Path="/5F0F427A/5FA65DD4" Ref="R49"  Part="1" 
F 0 "R40" H 3870 9646 50  0000 L CNN
F 1 "10k" H 3870 9555 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 3730 9600 50  0001 C CNN
F 3 "~" H 3800 9600 50  0001 C CNN
	1    3800 9600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 8450 3650 9250
Wire Wire Line
	3800 9750 3800 10450
Text Label 3800 10450 1    50   ~ 0
GNDA_REF
Wire Wire Line
	4100 8450 4100 8900
Text Label 4100 8900 1    50   ~ 0
LP_EN
Text Label 3800 9200 1    50   ~ 0
VOFS_LP
Text Label 2900 8600 1    50   ~ 0
VDD_PRE
Text HLabel 4100 8800 2    50   Input ~ 0
LP_EN
Text Notes 650  9000 0    50   ~ 0
PLL/CML powered from VDDA
Text Label 9800 8750 1    50   ~ 0
VDD_CML
Text Label 9800 9300 1    50   ~ 0
VDDA3
Text Label 9500 8750 1    50   ~ 0
VDD_PLL
Text Label 9500 9300 1    50   ~ 0
VDDA3
Text Label 11900 8750 1    50   ~ 0
VDD_CML
Text Label 11900 9300 1    50   ~ 0
VDDA3
Wire Wire Line
	9650 8450 9650 8700
Text Label 9650 8700 1    50   ~ 0
GND
Wire Wire Line
	9950 8450 9950 8700
Text Label 9950 8700 1    50   ~ 0
GND
Wire Wire Line
	10400 8450 10400 8700
Text Label 10400 8700 1    50   ~ 0
GND
Wire Wire Line
	10850 8450 10850 8700
Text Label 10850 8700 1    50   ~ 0
GND
Wire Wire Line
	11300 8450 11300 8700
Text Label 11300 8700 1    50   ~ 0
GND
Wire Wire Line
	11750 8450 11750 8700
Text Label 11750 8700 1    50   ~ 0
GND
Text HLabel 10100 9800 3    50   Input ~ 0
GTX0_N
Wire Wire Line
	10100 8450 10100 9800
Wire Wire Line
	10250 8450 10250 9800
Text HLabel 10250 9800 3    50   Input ~ 0
GTX0_P
Text HLabel 10550 9800 3    50   Input ~ 0
GTX1_N
Wire Wire Line
	10550 8450 10550 9800
Wire Wire Line
	10700 8450 10700 9800
Text HLabel 10700 9800 3    50   Input ~ 0
GTX1_P
Text HLabel 11000 9800 3    50   Input ~ 0
GTX2_N
Wire Wire Line
	11000 8450 11000 9800
Wire Wire Line
	11150 8450 11150 9800
Text HLabel 11150 9800 3    50   Input ~ 0
GTX2_P
Text HLabel 11450 9800 3    50   Input ~ 0
GTX3_N
Wire Wire Line
	11450 8450 11450 9800
Wire Wire Line
	11600 8450 11600 9800
Text HLabel 11600 9800 3    50   Input ~ 0
GTX3_P
Text Label 9200 9700 1    50   ~ 0
CMD_P
Text Label 9350 9700 1    50   ~ 0
CMD_N
Text Label 10100 9700 1    50   ~ 0
GTX0_N
Text Label 10250 9700 1    50   ~ 0
GTX0_P
Text Label 10550 9700 1    50   ~ 0
GTX1_N
Text Label 10700 9700 1    50   ~ 0
GTX1_P
Text Label 11000 9700 1    50   ~ 0
GTX2_N
Text Label 11150 9700 1    50   ~ 0
GTX2_P
Text Label 11450 9700 1    50   ~ 0
GTX3_N
Text Label 11600 9700 1    50   ~ 0
GTX3_P
Text Label 17000 8850 1    50   ~ 0
GNDD_REF
Wire Wire Line
	17300 8900 17300 9250
Text Label 17300 9250 1    50   ~ 0
VIN
$Comp
L Device:R R24
U 1 1 5FA65F29
P 17150 8750
AR Path="/5F0F4300/5FA65F29" Ref="R24"  Part="1" 
AR Path="/5F0F435C/5FA65F29" Ref="R33"  Part="1" 
AR Path="/5F0F439D/5FA65F29" Ref="R42"  Part="1" 
AR Path="/5F0F427A/5FA65F29" Ref="R51"  Part="1" 
F 0 "R42" H 17150 8900 50  0000 L CNN
F 1 "400" H 17150 8600 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 17080 8750 50  0001 C CNN
F 3 "~" H 17150 8750 50  0001 C CNN
	1    17150 8750
	1    0    0    -1  
$EndComp
Wire Wire Line
	17150 8450 17150 8600
Text Label 17150 9250 1    50   ~ 0
VIN
Text Label 17150 8600 1    50   ~ 0
REXTD
Wire Wire Line
	17150 8900 17150 9250
Text Notes 650  8850 0    50   ~ 0
REXTD = 535 (devide by 2 from RD53A)
$Comp
L Device:C C45
U 1 1 5FA65F2A
P 16750 8950
AR Path="/5F0F4300/5FA65F2A" Ref="C45"  Part="1" 
AR Path="/5F0F435C/5FA65F2A" Ref="C64"  Part="1" 
AR Path="/5F0F439D/5FA65F2A" Ref="C83"  Part="1" 
AR Path="/5F0F427A/5FA65F2A" Ref="C102"  Part="1" 
F 0 "C83" V 16700 8800 50  0000 C CNN
F 1 "100n" V 16600 8950 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 16788 8800 50  0001 C CNN
F 3 "~" H 16750 8950 50  0001 C CNN
	1    16750 8950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R23
U 1 1 5FA65F2B
P 16950 8950
AR Path="/5F0F4300/5FA65F2B" Ref="R23"  Part="1" 
AR Path="/5F0F435C/5FA65F2B" Ref="R32"  Part="1" 
AR Path="/5F0F439D/5FA65F2B" Ref="R41"  Part="1" 
AR Path="/5F0F427A/5FA65F2B" Ref="R50"  Part="1" 
F 0 "R41" V 16900 8800 50  0000 C CNN
F 1 "30k" V 17050 8950 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 16880 8950 50  0001 C CNN
F 3 "~" H 16950 8950 50  0001 C CNN
	1    16950 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	16750 9100 16750 9200
Wire Wire Line
	16750 9200 16850 9200
Wire Wire Line
	16950 9200 16950 9100
Wire Wire Line
	16850 9200 16850 9600
Connection ~ 16850 9200
Wire Wire Line
	16850 9200 16950 9200
Text Label 16850 9600 1    50   ~ 0
GNDD_REF
Wire Wire Line
	16750 8800 16750 8700
Wire Wire Line
	16950 8700 16950 8800
Text Label 16850 8700 1    50   ~ 0
VREFD
Wire Wire Line
	16750 8700 16850 8700
Wire Wire Line
	16850 8450 16850 8700
Connection ~ 16850 8700
Wire Wire Line
	16850 8700 16950 8700
Wire Wire Line
	17000 8450 17000 8850
Wire Wire Line
	18300 7850 19050 7850
Wire Wire Line
	18300 7700 19050 7700
Wire Wire Line
	18300 7550 19050 7550
Wire Wire Line
	18300 7400 19050 7400
Wire Wire Line
	18300 7250 19050 7250
Wire Wire Line
	18300 7100 19050 7100
Wire Wire Line
	18300 6950 19050 6950
Wire Wire Line
	18300 6800 19050 6800
Wire Wire Line
	18300 6650 19050 6650
Text Label 19050 8450 2    50   ~ 0
DATA_IN0_P
Text Label 19050 8150 2    50   ~ 0
DATA_IN0_N
Text Label 19050 8000 2    50   ~ 0
DATA_IN1_P
Text Label 19050 7850 2    50   ~ 0
DATA_IN1_N
Text Label 19050 7700 2    50   ~ 0
DATA_IN2_P
Text Label 19050 7550 2    50   ~ 0
DATA_IN2_N
Text Label 19050 7400 2    50   ~ 0
DATA_IN3_P
Text Label 19050 7250 2    50   ~ 0
DATA_IN3_N
Text Label 19050 7100 2    50   ~ 0
LVDS0_P
Text Label 19050 6950 2    50   ~ 0
LVDS0_N
Text Label 19050 6800 2    50   ~ 0
LVDS1_P
Text Label 19050 6650 2    50   ~ 0
LVDS1_N
Text HLabel 19050 8450 2    50   Input ~ 0
DATA_IN0_P
Text HLabel 19050 8150 2    50   Input ~ 0
DATA_IN0_N
Text HLabel 19050 8000 2    50   Input ~ 0
DATA_IN1_P
Text HLabel 19050 7700 2    50   Input ~ 0
DATA_IN2_P
Text HLabel 19050 7400 2    50   Input ~ 0
DATA_IN3_P
Text HLabel 19050 7850 2    50   Input ~ 0
DATA_IN1_N
Text HLabel 19050 7550 2    50   Input ~ 0
DATA_IN2_N
Text HLabel 19050 7250 2    50   Input ~ 0
DATA_IN3_N
Text HLabel 19050 7100 2    50   Input ~ 0
LVDS0_P
Text HLabel 19050 6800 2    50   Input ~ 0
LVDS1_P
Text HLabel 19050 6950 2    50   Input ~ 0
LVDS0_N
Text HLabel 19050 6650 2    50   Input ~ 0
LVDS1_N
Wire Notes Line
	2250 10500 550  10500
Text Notes 650  9850 0    50   ~ 0
GNDA_REF and GNDD_REF are reference\n GNDs that the chip provides\n
Text Notes 650  9150 0    50   ~ 0
LP_EN AC coupled on the module
Wire Wire Line
	1100 3100 1100 2350
Wire Wire Line
	1100 2350 1950 2350
Connection ~ 1100 3100
Wire Wire Line
	1100 5350 1100 6100
Wire Wire Line
	1100 6100 1950 6100
Connection ~ 1100 5350
Wire Wire Line
	4700 9550 5450 9550
Wire Wire Line
	4700 8700 4700 9550
Connection ~ 5450 9550
Wire Wire Line
	7700 9550 8450 9550
Wire Wire Line
	8450 8700 8450 9550
Connection ~ 7700 9550
Wire Wire Line
	12500 9550 13250 9550
Wire Wire Line
	12500 8700 12500 9550
Connection ~ 13250 9550
Wire Wire Line
	15500 9550 16250 9550
Wire Wire Line
	16250 8700 16250 9550
Connection ~ 15500 9550
Wire Wire Line
	19400 4700 19400 5450
Wire Wire Line
	18550 5450 19400 5450
Connection ~ 19400 4700
Wire Wire Line
	19400 1700 19400 2450
Wire Wire Line
	18550 1700 19400 1700
Wire Wire Line
	19400 2450 19150 2450
Connection ~ 19400 2450
Wire Wire Line
	18300 1100 19350 1100
$Comp
L Device:C C36
U 1 1 5FA65DCE
P 1900 700
AR Path="/5F0F4300/5FA65DCE" Ref="C36"  Part="1" 
AR Path="/5F0F435C/5FA65DCE" Ref="C55"  Part="1" 
AR Path="/5F0F439D/5FA65DCE" Ref="C74"  Part="1" 
AR Path="/5F0F427A/5FA65DCE" Ref="C93"  Part="1" 
F 0 "C74" H 2015 746 50  0000 L CNN
F 1 "100n" H 2015 655 50  0000 L CNN
F 2 "Capacitors_SMD:C_0201_NoSilk" H 1938 550 50  0001 C CNN
F 3 "~" H 1900 700 50  0001 C CNN
	1    1900 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 550  600  550 
Text Label 600  550  0    50   ~ 0
GND
Text Notes 18150 650  0    50   ~ 0
optional\n(or a wirebond to the GND)\n
$Comp
L Device:C C46
U 1 1 5FA65F25
P 18550 800
AR Path="/5F0F4300/5FA65F25" Ref="C46"  Part="1" 
AR Path="/5F0F435C/5FA65F25" Ref="C65"  Part="1" 
AR Path="/5F0F439D/5FA65F25" Ref="C84"  Part="1" 
AR Path="/5F0F427A/5FA65F25" Ref="C103"  Part="1" 
F 0 "C84" H 18665 846 50  0000 L CNN
F 1 "100n" H 18665 755 50  0000 L CNN
F 2 "Capacitors_SMD:C_0201_NoSilk" H 18588 650 50  0001 C CNN
F 3 "~" H 18550 800 50  0001 C CNN
	1    18550 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	19350 650  18550 650 
Text Label 19350 650  2    50   ~ 0
GND
Wire Wire Line
	18300 950  18550 950 
Text Notes 2850 1500 0    50   ~ 0
Only VMUX connected\n(IMUX can be read through the VMUX)
Wire Wire Line
	600  1600 1100 1600
Wire Wire Line
	1400 1600 1600 1600
Wire Wire Line
	1600 1450 1600 1600
Wire Wire Line
	1600 1450 2200 1450
Wire Wire Line
	2200 6700 1950 6700
Wire Wire Line
	1950 6700 1950 6850
Wire Wire Line
	2200 6850 1950 6850
Wire Wire Line
	1950 6700 1950 6550
Wire Wire Line
	1950 6550 2200 6550
Connection ~ 1950 6700
Text Label 1950 6700 2    50   ~ 0
GND
Text Notes 900  6800 0    50   ~ 0
Connect to GND\n(internal pull-down 40k)\n
Text Notes 17350 8850 0    50   ~ 0
solder jumper to enable \npowering with shunt
$Comp
L Device:R R18
U 1 1 5FA65DCD
P 1250 1800
AR Path="/5F0F4300/5FA65DCD" Ref="R18"  Part="1" 
AR Path="/5F0F435C/5FA65DCD" Ref="R27"  Part="1" 
AR Path="/5F0F439D/5FA65DCD" Ref="R36"  Part="1" 
AR Path="/5F0F427A/5FA65DCD" Ref="R45"  Part="1" 
F 0 "R36" V 1200 1650 50  0000 C CNN
F 1 "5k" V 1350 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 1180 1800 50  0001 C CNN
F 3 "~" H 1250 1800 50  0001 C CNN
	1    1250 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	1400 1800 1650 1800
Wire Wire Line
	1650 1800 1650 1600
Wire Wire Line
	1650 1600 2200 1600
Text Label 600  1800 0    50   ~ 0
GNDA_REF_A
Wire Wire Line
	600  1800 1100 1800
Text Notes 650  9600 0    50   ~ 0
Input/output capacitance:\n- 4 x 4.7u for Vin (analog + digital)\n- 4 x 560n for Vdda\n- 4 x 560n for Vddd
$Comp
L Device:R R25
U 1 1 5FA65DD3
P 17300 8750
AR Path="/5F0F4300/5FA65DD3" Ref="R25"  Part="1" 
AR Path="/5F0F435C/5FA65DD3" Ref="R34"  Part="1" 
AR Path="/5F0F439D/5FA65DD3" Ref="R43"  Part="1" 
AR Path="/5F0F427A/5FA65DD3" Ref="R52"  Part="1" 
F 0 "R43" H 17300 8900 50  0000 L CNN
F 1 "0" H 17300 8600 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 17230 8750 50  0001 C CNN
F 3 "~" H 17300 8750 50  0001 C CNN
	1    17300 8750
	1    0    0    -1  
$EndComp
Text HLabel 3800 9150 2    50   Input ~ 0
VOFS_LP
Text Label 3650 9200 1    50   ~ 0
VOFS_OUT
Wire Wire Line
	3800 8450 3800 9450
Text Label 3950 9050 1    50   ~ 0
VOFS_IN
Text HLabel 3950 9050 2    50   Input ~ 0
VOFS_IN
Text HLabel 3650 9250 2    50   Input ~ 0
VOFS_OUT
Text HLabel 1550 7450 0    50   Input ~ 0
CHIP_ID2
Wire Wire Line
	2200 7450 1550 7450
Text Label 1950 7250 2    50   ~ 0
GND
Wire Wire Line
	2200 7600 1550 7600
Text HLabel 1550 7600 0    50   Input ~ 0
CHIP_ID3
Wire Wire Line
	9500 8450 9500 9300
Wire Wire Line
	9800 8450 9800 9300
Wire Wire Line
	11900 8450 11900 9300
Wire Notes Line
	550  8450 550  10500
Wire Notes Line
	2250 8450 2250 10500
Wire Notes Line
	550  8450 2250 8450
Wire Wire Line
	1950 4150 1950 4300
Connection ~ 1950 4150
Connection ~ 1950 4300
Wire Wire Line
	1650 4300 1950 4300
Wire Wire Line
	1100 3100 1100 4300
Wire Wire Line
	1350 4300 1100 4300
Connection ~ 1100 4300
Wire Wire Line
	1100 4300 1100 5350
Text Label 1650 4300 0    50   ~ 0
VIN
Text Label 1100 4300 0    50   ~ 0
GND
Wire Wire Line
	6500 8700 6650 8700
Connection ~ 6500 8700
Connection ~ 6650 8700
Wire Wire Line
	6650 9000 6650 8700
Wire Wire Line
	6650 9300 6650 9550
Connection ~ 6650 9550
Wire Wire Line
	5450 9550 6650 9550
Wire Wire Line
	6650 9550 7700 9550
Text Label 6650 9000 1    50   ~ 0
VIN
Text Label 6650 9550 1    50   ~ 0
GND
Wire Wire Line
	14300 8700 14450 8700
Connection ~ 14300 8700
Connection ~ 14450 8700
Wire Wire Line
	14450 8700 14450 9000
Wire Wire Line
	14450 9300 14450 9550
Connection ~ 14450 9550
Wire Wire Line
	14450 9550 15500 9550
Wire Wire Line
	13250 9550 14450 9550
Text Label 14450 9000 1    50   ~ 0
VIN
Text Label 14450 9550 1    50   ~ 0
GND
Wire Wire Line
	18550 3500 18550 3650
Connection ~ 18550 3500
Connection ~ 18550 3650
Wire Wire Line
	18550 3500 18850 3500
Wire Wire Line
	19150 3500 19400 3500
Connection ~ 19400 3500
Wire Wire Line
	19400 3500 19400 4700
Wire Wire Line
	19400 2450 19400 3500
Text Label 18850 3500 2    50   ~ 0
VIN
Text Label 19400 3500 2    50   ~ 0
GND
Text Label 1950 850  0    50   ~ 0
DET_GRD0
Text Label 1950 1600 0    50   ~ 0
IMUX
Text Label 1700 7450 0    50   ~ 0
CHIP_ID2
Text Label 1700 7600 0    50   ~ 0
CHIP_ID3
Text Label 17300 8550 0    50   ~ 0
VDD_SHUNT
Text Label 18300 950  0    50   ~ 0
DET_GRD1
Wire Wire Line
	17300 8450 17300 8600
Wire Wire Line
	700  10200 850  10200
$EndSCHEMATC
